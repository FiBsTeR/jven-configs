# bash shell config
# Author: Justin Venezuela (jven@mit.edu)

export LIBGL_ALWAYS_INDIRECT=1

# use l for ls
alias l='ls -G'

# custom colored prompt
JVENNAME=$'meredith'
GREEN=$'\e[0;32m'
RED=$'\e[0;31m'
YELLOW=$'\e[0;33m'
CYAN=$'\e[0;36m'
WHITE=$'\e[0;37m'
# if we're in a screen, show the name of the screen
if [ "$STY" ]; then
  PS1='\[$GREEN\]\u \[$CYAN\]($JVENNAME \[$YELLOW\]$STY\[$CYAN\])\[$GREEN\]: \[$RED\]\w \[$YELLOW\]$ \[$WHITE\]'
else
  PS1='\[$GREEN\]\u \[$CYAN\]($JVENNAME)\[$GREEN\]: \[$RED\]\w \[$YELLOW\]$ \[$WHITE\]'
fi
color_prompt=yes

# allow good Python package naming in Repositories
export PYTHONPATH=$PYTHONPATH:~/Repositories
