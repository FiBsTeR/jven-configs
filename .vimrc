" vim config file
" Author: Justin Venezuela (jven@mit.edu)

" line numbering
set nu

" syntax coloring
syntax on

" indentation
filetype indent on
set autoindent

" case insensitive search
set ic

" highlight search
set hls

" soft wrap text
set lbr

" colorscheme
colorscheme delek

" tab width stuff
set tabstop=2
set shiftwidth=2
set expandtab
set showmode

" right hand margin
set colorcolumn=81,101

" show tabs and trailing whitespace
set list listchars=tab:\#;,trail:\#

" ctrl+p: http://kien.github.io/ctrlp.vim/
set runtimepath^=~/.vim/bundle/ctrlp.vim

" colors
hi ColorColumn ctermbg=LightMagenta
hi Comment cterm=bold
hi LineNr cterm=bold
hi ModeMsg ctermfg=4
hi StatusLine cterm=bold ctermfg=3 ctermbg=5
hi StatusLineNC ctermfg=3 ctermbg=5
hi TabLineSel cterm=bold,underline ctermbg=Yellow

" always show current mode
set laststatus=2
